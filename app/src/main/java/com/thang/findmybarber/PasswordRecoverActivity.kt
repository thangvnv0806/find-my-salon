package com.thang.findmybarber

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.thang.findmybarber.model.User
import com.thang.findmybarber.repository.AppRepository
import kotlinx.android.synthetic.main.activity_password_recover.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class PasswordRecoverActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password_recover)
        btn_update_password.setOnClickListener {
            if (validate()) {
                MainScope().launch {
                    AppRepository().changePassword(
                        User(
                            id = intent.getIntExtra("userId", 0),
                            password = this@PasswordRecoverActivity.input_password.text.toString()
                        )
                    )
                    val intent = Intent(this@PasswordRecoverActivity, LoginActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                }
            }
        }

    }

    fun validate(): Boolean {
        var valid = true
        val passwordConfirm: String = input_password_confirm.getText().toString()
        val password: String = input_password.getText().toString()
        if (passwordConfirm.isEmpty()) {
            input_password_confirm.setError("Xác nhận mật khẩu không thể trống")
            valid = false
        } else {
            input_password_confirm.setError(null)
        }
        if (password.isEmpty()) {
            input_password.setError("Mật khẩu không thể trống")
            valid = false
        } else {
            input_password.setError(null)
        }
        if (!password.contentEquals(passwordConfirm)) {
            input_password_confirm.setError("Mật khẩu và xác nhận mật khẩu không khớp")
        }
        return valid
    }
}
