package com.thang.findmybarber

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.thang.findmybarber.repository.AppRepository
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class ForgotPasswordActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        btn_forgot_password.setOnClickListener {
            MainScope().launch {
                val user = AppRepository().getUserByUsername(input_username.text.toString())
                if (user != null) {
                    startActivity(Intent(this@ForgotPasswordActivity, PinActivity::class.java))
                } else {
                    input_username.error = "Không tìm thấy tên đăng nhập"
                }
            }

        }
        link_login.setOnClickListener {
            finish()
        }
    }
}
