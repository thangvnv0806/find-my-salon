package com.thang.findmybarber.model

import java.util.*

data class Slot(
    val time: Calendar,
    var status: SlotStatus = SlotStatus.NOT_SELECTED,
    val discount: Int? = null
)

enum class SlotStatus {
    UNSELECTABLE, SELECTED, NOT_SELECTED
}
