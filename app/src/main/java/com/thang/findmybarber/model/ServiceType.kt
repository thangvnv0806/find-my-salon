package com.thang.findmybarber.model

import java.io.Serializable

class ServiceType (
    val name: String,
    val img: Int,
    val id: Int? = null
): Serializable