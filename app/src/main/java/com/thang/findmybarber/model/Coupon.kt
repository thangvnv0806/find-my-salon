package com.thang.findmybarber.model

import java.util.*

data class Coupon(
    var id: Int? = null,
    var name: String? = null,
    var shopName: String? = null,
    var shopId: Int? = null,
    var serviceName: String? = null,
    var serviceId: Int? = null,
    var discountPrice: Double? = null,
    var deadline: Calendar? = null,
    var img: Int? = null
)