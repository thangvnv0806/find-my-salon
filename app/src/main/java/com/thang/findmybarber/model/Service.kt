package com.thang.findmybarber.model

import com.thang.findmybarber.apiclient.AppRetrofit
import com.thang.findmybarber.repository.AppRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import okhttp3.Dispatcher
import java.io.Serializable

data class Service(
    var store_id: Int,
    val type: Int? = null,
    var id: Int,
    var name: String,
    var price: Int,
    val rating: Double?,
    var ratingNumber: Int,
    var discount: Int,
    var promotion_price: Int? = null,
    var promotion_time: String? = null,
    var duration: Int? = null,
    var detail: String? = null,
    val image: String? = null,
//    @Transient
    var shop: Store? = null,
    @Transient
    var dummy_image: Int? = null
) : Serializable

//"id": 391,
//        "name": "Cắt tóc nữ",
//        "detail": "Việc tạo kiểu cho mái tóc sẽ làm bạn trông quyến rũ hơn và thể hiện phong cách riêng. Đây là cách tuyệt vời để làm nổi bật tính cách của bạn. Các kiểu tóc thông thường rất đa dạng và bạn nên tìm kiểu phù hợp nhất với mình.",
//        "type": 1,
//        "rating": 4.4,
//        "duration": 30,
//        "price": 100000,
//        "discount": 25,
//        "promotion_price": 75000,
//        "promotion_time": "11:00,12:00,13:00,14:00",
//        "image": "aHR0cHM6Ly93d3cubm9pdG9jdGhhb3RheS5jb20vaW1nL3RodW1ibmFpbHMvNTAwL2JhdF9taV9kaWFfY2hpX2NhdF90b2NfbnVfZGVwMjAxNjExMDIxNzE3MDguanBn",
//        "store_id": 101
