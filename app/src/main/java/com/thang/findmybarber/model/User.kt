package com.thang.findmybarber.model

data class User (
    val id: Int? = null,
    val username: String? = null,
    val password: String? = null,
    val role: String? = null,
    val name: String? = null,
    val phone_no: String? = null,
    val address: String? = null
)
//{
//    "id": 151,
//    "email": "admin@mail.com",
//    "password": "111111",
//    "role": "admin",
//    "name": "123",
//    "phone_no": "4123",
//    "address": "1wqw"
//}