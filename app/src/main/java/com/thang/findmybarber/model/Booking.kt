package com.thang.findmybarber.model

import java.io.Serializable
import java.util.*

data class Booking (
//    @Transient
    var service: Service? = null,
    val service_id: Int,
    val status: String,
    val service_time: String,
    val created_time: String,
    val userId: Int,
    val price: Int
): Serializable {
    companion object {
        //pending, done, cancel
        const val PENDING = "pending"
        const val DONE = "done"
        const val CANCEL = "cancel"
    }
}