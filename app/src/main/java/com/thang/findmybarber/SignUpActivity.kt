package com.thang.findmybarber

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.thang.findmybarber.apiclient.AppRetrofit
import com.thang.findmybarber.model.User
import com.thang.findmybarber.repository.AppRepository
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class SignUpActivity : AppCompatActivity() {

    lateinit var name: String
    lateinit var username: String
    lateinit var password: String
    lateinit var phone: String
    lateinit var address: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        btn_signup.setOnClickListener {
            if (validate()) {
                MainScope().launch(Dispatchers.Main) {
                    loading?.show()
                    withContext(Dispatchers.IO) {
                        AppRepository().signUp(
                            User(
                                name = name,
                                username = username,
                                password = password,
                                phone_no = phone,
                                address = address,
                                role = "member"
                            )
                        )
                    }
                    setResult(RESULT_OK, null)
                    finish()
                    loading?.hide()
                }
            }
        }

        link_login.setOnClickListener {
            finish()
        }
    }

    fun validate(): Boolean {
        var valid = true
        name = input_name.text.toString()
        username = input_username.text.toString()
        phone = input_phone.text.toString()
        password = input_password.text.toString()
        address = input_address.text.toString()
        if (name.isEmpty()) {
            input_name.error = "Tên không thể trống"
            valid = false
        } else {
            input_name.error = null
        }
        if (username.isEmpty()) {
            input_username.error = "Tên đăng nhập không thể trống"
            valid = false
        } else {
            input_username.error = null
        }
        if (phone.isEmpty()) {
            input_phone.error = "Phone is empty"
            valid = false
        } else {
            input_phone.error = null
        }
        if (password.isEmpty()) {
            input_password.error = "Mật khẩu không thể trống"
            valid = false
        } else {
            input_password.error = null
        }
        return valid
    }
}
