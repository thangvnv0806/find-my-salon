package com.thang.findmybarber.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView
import com.thang.findmybarber.R
import com.thang.findmybarber.ServiceDetailActivity
import com.thang.findmybarber.formatPrice
import com.thang.findmybarber.model.Coupon
import kotlinx.android.synthetic.main.discount_item.view.*

class DiscountAdapter(private val discounts: MutableList<Coupon>) :
    RecyclerView.Adapter<DiscountAdapter.DiscountViewHolder>() {
    val checkBoxs = ArrayList<CheckBox>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiscountViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.discount_item, parent, false)
        return DiscountViewHolder(view)
    }

    override fun getItemCount(): Int = discounts.size

    override fun onBindViewHolder(holder: DiscountViewHolder, position: Int) {
        val view = holder.itemView
        val discount = discounts[position]

        view.run {
            discount_item_name.text = discount.name
            discount_item_amount.text = "Giảm ${formatPrice(discount.discountPrice?.toInt())}đ"
            checkBoxs.add(discount_item_select)
            discount_item_select.setOnClickListener {
                if (!it.isSelected) {
                    for (checkbox in checkBoxs) {
                        checkbox.isSelected = false
                    }
                }
                it.isSelected = !it.isSelected
                notifyDataSetChanged()
            }

            setOnClickListener {
                val intent = Intent(it.context, ServiceDetailActivity::class.java)
                it.context.startActivity(intent)
            }
        }
    }

    class DiscountViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}