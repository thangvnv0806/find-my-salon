package com.thang.findmybarber.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.thang.findmybarber.R
import com.thang.findmybarber.SalonActivity
import com.thang.findmybarber.model.Store
import com.thang.findmybarber.repository.AppRepository
import kotlinx.android.synthetic.main.shop_card_view.view.*

class ShopAdapter(private val shops: MutableList<Store>, private val layout: Int? = null) :
    RecyclerView.Adapter<ShopAdapter.ShopViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShopViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(layout?: R.layout.shop_card_view, parent, false)
        return ShopViewHolder(view)
    }

    override fun getItemCount(): Int = shops.size

    override fun onBindViewHolder(holder: ShopViewHolder, position: Int) {
        val shop = shops[position]
        holder.itemView.run {
            shop_name.text = shop.name
            shop_address.text = shop.address
            shop_distance.text = "${AppRepository.formatDistance(shop.distance)}km"
            shop_rating_bar.rating = shop.rating.toFloat()
//            shop_rating_text.text = "(${shop.ratingNumber})"
//            shop_price_text.text = "Từ ${shop.basePrice}đ"
//            if (shop.topDiscount != null) shop_top_discount.text = "Giảm đến ${shop.topDiscount}"
//            else shop_top_discount.visibility = View.GONE
            shop_image.setImageResource(shop.shopImg)

            setOnClickListener {
                val intent = Intent(context, SalonActivity::class.java)
                intent.putExtra("salon", shop)
                context.startActivity(intent)
            }
        }
    }

    class ShopViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}