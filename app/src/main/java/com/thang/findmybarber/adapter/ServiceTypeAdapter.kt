package com.thang.findmybarber.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.thang.findmybarber.R
import com.thang.findmybarber.model.ServiceType
import kotlinx.android.synthetic.main.service_type_horizontal.view.*

class ServiceTypeAdapter(
    private val serviceTypes: MutableList<ServiceType>,
    private val layout: Int = R.layout.service_type_vertical,
    val onItemClick: (ServiceType) -> Unit
) : Adapter<ServiceTypeAdapter.ServiceTypeViewHolder>() {

    companion object {
        @Volatile
        private var selectedPos = -1
    }

    class ServiceTypeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceTypeViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(layout, parent, false)
        if (viewType == 1) view.selected_image?.visibility = View.VISIBLE
        else view.selected_image?.visibility = View.INVISIBLE
        return ServiceTypeViewHolder(view)
    }

    override fun getItemCount(): Int = serviceTypes.size

    override fun getItemViewType(position: Int): Int {
        if (position == selectedPos) return 1
        return 0
    }

    override fun onBindViewHolder(holder: ServiceTypeViewHolder, position: Int) {
        val serviceType = serviceTypes[position]
        holder.itemView.service_type_name_text.text = serviceType.name
        holder.itemView.service_type_image.setImageResource(serviceType.img)
        holder.itemView.setOnClickListener {
            selectServiceType(serviceType.id!!)
            onItemClick(serviceType)
        }
    }

    fun selectServiceType(serviceTypeId: Int) {
        val index = serviceTypes.indexOfFirst { it.id == serviceTypeId }
        selectedPos = index
        notifyDataSetChanged()
    }
}