package com.thang.findmybarber.apiclient

import com.thang.findmybarber.adapter.RatingAdapter
import com.thang.findmybarber.model.*
import retrofit2.http.*

interface AppApiClient {
    @POST("api/v1/login")
    suspend fun login(@Body user: User): User?
    @GET("api/v1/getUserByUsername/{username}")
    suspend fun getUserByUsername(@Path("username") username: String): User?
    @POST("api/v1/newUser")
    suspend fun signUp(@Body user: User)
    @PUT("api/v1/updateUser")
    suspend fun updateUser(@Body user: User)
    @GET("api/v1/getAllService")
    suspend fun getServices(): List<Service>
    @GET("api/v1/getStoreService/{storeId}")
    suspend fun getServices(@Path("storeId") storeId: Int): List<Service>
    @GET("api/v1/getServiceByType/{typeId}")
    suspend fun getServicesByType(@Path("typeId") typeId: Int): List<Service>
    @GET("api/v1/getStore/{id}")
    suspend fun getStore(@Path("id") storeId: Int): Store
    @GET("api/v1/getServiceByID/{id}")
    suspend fun getService(@Path("id") serviceId: Int): Service
    @GET("api/v1/getUserByRole/{role}")
    suspend fun getUsers(@Path("role") role: String): List<User>
    @GET("api/v1/getBooking/{userId}")
    suspend fun getBooking(@Path("userId") userId: Int): List<Booking>
    @POST("api/v1/newBooking")
    suspend fun book(@Body booking: Booking)
    @GET("api/v1/getServiceReview/{serviceId}")
    suspend fun getServiceReview(@Path("serviceId") serviceId: Int): List<Review>
}