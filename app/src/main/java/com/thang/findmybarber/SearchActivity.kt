package com.thang.findmybarber

import android.app.SearchManager
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.thang.findmybarber.adapter.ServiceAdapter
import com.thang.findmybarber.dialog.SortDialog
import com.thang.findmybarber.dialog.SortDialog.Sort.*
import com.thang.findmybarber.repository.AppRepository
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class SearchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        // Verify the action and get the query
        if (Intent.ACTION_SEARCH == intent.action) {
            intent.getStringExtra(SearchManager.QUERY)?.also { query ->
//                doMySearch(query)
                search_result.text = "Đây là kết quả của: $query"
                MainScope().launch {
                    val services = AppRepository().getServices().filter {
                        it.name.contains(query)
                    }.toMutableList()
                    val serviceAdapter = ServiceAdapter(services, R.layout.service_item_store)

                    search_service_rcv.adapter = serviceAdapter

                    sort.setOnClickListener {
                        SortDialog {
                            when (it) {
                                PRICE.sortId -> services.sortBy { it.price }
                                RATE.sortId -> services.sortByDescending { it.price }
                                DISCOUNT.sortId -> {
                                    services.sortByDescending { service ->
                                        service.price - (service.promotion_price
                                            ?: service.price)
                                    }
                                }
                                NEAR.sortId -> services.sortBy { "%.2".format(it.shop?.distance) }
                            }
                            serviceAdapter.notifyDataSetChanged()
                        }.show(supportFragmentManager, "sort")
                    }
                }
            }
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
