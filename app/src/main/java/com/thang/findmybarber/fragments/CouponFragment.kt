package com.thang.findmybarber.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.thang.findmybarber.R
import com.thang.findmybarber.adapter.CouponAdapter
import com.thang.findmybarber.model.Coupon
import com.thang.findmybarber.repository.AppRepository
import kotlinx.android.synthetic.main.fragment_coupon.*
import kotlinx.android.synthetic.main.fragment_coupon.view.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class CouponFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_coupon, container, false)
        val coupons = AppRepository().getCoupons()
        view.coupon_rcv.adapter = CouponAdapter(coupons)
        return view
    }
}
