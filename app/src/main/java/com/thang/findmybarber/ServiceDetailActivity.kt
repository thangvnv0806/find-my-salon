package com.thang.findmybarber

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.LocationServices
import com.squareup.picasso.Picasso
import com.thang.findmybarber.adapter.RatingAdapter
import com.thang.findmybarber.adapter.ServiceSlotAdapter
import com.thang.findmybarber.model.*
import com.thang.findmybarber.repository.AppRepository
import kotlinx.android.synthetic.main.activity_service_detail.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class ServiceDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_detail)
        val service = intent.getSerializableExtra("service") as Service
                MainScope().launch {
                if (service.shop == null) service.shop = AppRepository().getStore(service.store_id)
                service_name.text = service.name
                service.shop?.let { shop_address.text = it.address }
                service_duration.text = "${service.duration} phút"
                service_description.text = service.detail
                service_price.text = "${formatPrice(service.price)}đ"
                rating.text = service.rating.toString()
                rating_number?.text = service.ratingNumber.toString()

//            if (service.promotion_price != 0) {
//                service_price_crossed.text = "${service.price}đ"
//                service_price.text = "${service.promotion_price}đ"
//            } else {
//                service_price_crossed.visibility = View.GONE
//                service_price.text = "${service.price}đ"
//            }
            if (!service.image.isNullOrEmpty()){
                val image = Picasso.get().load(service.image)
                image?.into(service_image)
            } else {
                service.dummy_image?.let { service_image.setImageResource(it) }
            }
            service_price_crossed.visibility = View.GONE
            service_price.text = "${formatPrice(service.price)}đ"
            btn_shop.text = "Xem thông tin ${service.shop?.name}"
            btn_shop.setOnClickListener {
                startActivity(Intent(this@ServiceDetailActivity, SalonActivity::class.java).apply {
                    putExtra("salon", service.shop)
                })
            }
        }
        MainScope().launch {
            val reviews = AppRepository().getReviews(service.id)
            rating_rcv.adapter = RatingAdapter(reviews)
        }
        service_date_picker.apply {
            minDate = Calendar.getInstance().timeInMillis
            maxDate =
                Calendar.getInstance().apply { add(Calendar.DAY_OF_MONTH, 7) }.timeInMillis
        }
        MainScope().launch {
            if (service.shop == null) service.shop = AppRepository().getStore(service.store_id)
            val slots = AppRepository().getSlot(service)
            val adapter = ServiceSlotAdapter(slots) {
                if (service.promotion_price != 0 && it.discount != 0 && it.discount != null) {
                    service_price_crossed.text = "${formatPrice(service.price)}đ"
                    service_price.text = "${formatPrice(service.promotion_price)}đ"
                    service_price_crossed.visibility = View.VISIBLE
                } else {
                    service_price_crossed.visibility = View.GONE
                    service_price.text = "${formatPrice(service.price)}đ"
                }
            }
            service_slots_rcv.adapter = adapter
            service.rating?.let { rating_bar.rating = it.toFloat() }

            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            service_order_btn.setOnClickListener {
                val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                val selectedSlot = Calendar.getInstance().apply {
                    set(
                        service_date_picker.year,
                        service_date_picker.month,
                        service_date_picker.dayOfMonth,
                        slots[adapter.selectedPos].time.get(Calendar.HOUR_OF_DAY),
                        slots[adapter.selectedPos].time.get(Calendar.MINUTE)
                    )
                }
                val userId =
                    getSharedPreferences(packageName, Context.MODE_PRIVATE).getInt("userId", 0)
                val booking = Booking(
                    service_id = service.id,
                    status = Booking.PENDING,
                    created_time = dateFormat.format(Calendar.getInstance().time),
                    userId = userId, service_time = dateFormat.format(selectedSlot.time),
                    price = if (slots[adapter.selectedPos].discount == null) service.price else service.promotion_price!!,
                    service = service
                )
                MainScope().launch {
                    AppRepository().book(booking)
                    startActivity(Intent(this@ServiceDetailActivity, SuccessActivity::class.java).apply {
                        putExtra("booking", booking)
                    })
                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
