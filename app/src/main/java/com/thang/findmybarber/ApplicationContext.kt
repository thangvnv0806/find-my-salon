package com.thang.findmybarber

import android.content.Context

class ApplicationContext private constructor() {
    private var appContext: Context? = null
    fun init(context: Context) {
        if (appContext == null) {
            appContext = context
        }
    }

    private val context: Context?
        private get() = appContext

    companion object {
        fun get(): Context? {
            return instance!!.context
        }

        var instance: ApplicationContext? = null
            get() = if (field == null) ApplicationContext().also { field = it } else field
            private set
    }
}