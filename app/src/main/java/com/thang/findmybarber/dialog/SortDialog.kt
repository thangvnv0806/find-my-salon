package com.thang.findmybarber.dialog

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.thang.findmybarber.R
import kotlinx.android.synthetic.main.dialog_sort.view.*

class SortDialog(val sort: (id: Int?) -> Unit) : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater
            val view = inflater.inflate(R.layout.dialog_sort, null)
            var selectedId: Int? = null
            view.radio_group.setOnCheckedChangeListener { group, checkedId ->
                selectedId= checkedId
            }
            // Inflate and set the layout for the dialog
            // Pass null as the parent view because its going in the dialog layout
            builder.setView(view)
                // Add action buttons
                .setPositiveButton("Sắp xếp") { _, _ ->
                    sort(selectedId)
                }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    enum class Sort(val sortId: Int) {
        PRICE(R.id.radio_price),
        RATE(R.id.radio_rate),
        NEAR(R.id.radio_near),
        DISCOUNT(R.id.radio_discount),
    }
}